## Ejercicio: Sistema de Reservación de Boletos de Tren usando Listas Enlazadas y FastAPI

### Contexto
Una compañía de ferrocarriles desea implementar un nuevo sistema para gestionar las reservaciones de boletos de tren. Dado que los trenes pueden tener un número variable de vagones y cada vagón un número variable de asientos, la compañía ha decidido utilizar listas enlazadas para modelar esta estructura dinámicamente. Además, para facilitar la interacción con el sistema, se implementarán servicios web utilizando FastAPI.

### Uso de Visualgo
Para apoyar el diseño y la comprensión de las listas enlazadas, se recomienda el uso de [Visualgo](https://visualgo.net/en). Visualgo es una herramienta interactiva en línea que permite visualizar algoritmos y estructuras de datos, incluyendo listas enlazadas. Los estudiantes pueden usar Visualgo para:
- Comprender cómo funcionan las listas enlazadas de manera visual.
- Experimentar con diferentes operaciones en listas enlazadas, como inserciones y eliminaciones.
- Visualizar la diferencia entre listas enlazadas simples y dobles.

### Objetivo
Tu tarea es diseñar e implementar un sistema de reservación de boletos que permita añadir y eliminar vagones de un tren, así como reservar y cancelar asientos dentro de estos vagones. Debes utilizar listas enlazadas para manejar la variabilidad en el número de vagones y asientos. Este sistema debe estar implementado como un servicio con propósitos generales en FastAPI, proporcionando endpoints para la reserva, cancelación y verificación de disponibilidad de asientos.


#### FastAPI Endpoints (Prototipo - Taller General):
- **Reserva de Asientos:** Un endpoint para reservar un asiento en un vagón específico.
- **Cancelación de Reserva:** Un endpoint para cancelar una reservación de asiento.
- **Verificación de Disponibilidad:** Un endpoint para verificar la disponibilidad de asientos en el tren.

#### Visualización:
- Implementar una función para visualizar la configuración actual del tren, mostrando el número de vagones, el número de asientos por vagón y el estado de cada asiento (libre o reservado). Este debe ser accesible mediante un endpoint de FastAPI.

### Desafíos Adicionales (Opcionales)
- Implementar una función para encontrar el vagón con la mayor cantidad de asientos libres.
- Añadir una función para reservar múltiples asientos consecutivos para grupos.

### Entrega
Tu código debe estar bien comentado y estructurado. Se valorará la eficiencia en la manipulación de las listas enlazadas, así como la claridad en la implementación de las funciones solicitadas y los endpoints en FastAPI. Asegúrate de incluir casos de prueba para demostrar la funcionalidad de tu sistema, el codigo debe quedar en Git.
